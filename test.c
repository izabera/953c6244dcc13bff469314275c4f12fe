#define _GNU_SOURCE
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "s.h"
#include "sds.h"

int main() {
  struct timespec times[4];

  enum { count = 1000000 };

     s  *  s_strings = malloc(count * sizeof(   s ));
   sds  *sds_strings = malloc(count * sizeof( sds ));
  char **  c_strings = malloc(count * sizeof(char*));

  memset(  s_strings, 1, count * sizeof(   s ));
  memset(sds_strings, 1, count * sizeof( sds ));
  memset(  c_strings, 1, count * sizeof(char*));



  clock_gettime(CLOCK_MONOTONIC, &times[0]);

  for (int i = 0; i < count; i++)
    s_itos(&s_strings[i], i);

  clock_gettime(CLOCK_MONOTONIC, &times[1]);

  for (int i = 0; i < count; i++)
    sds_strings[i] = sdsfromlonglong(i);

  clock_gettime(CLOCK_MONOTONIC, &times[2]);

  for (int i = 0; i < count; i++)
    asprintf(&c_strings[i], "%d", i);

  clock_gettime(CLOCK_MONOTONIC, &times[3]);



  printf("    s: %f\n",
      (double) (times[1].tv_sec  - times[0].tv_sec) +
      (double) (times[1].tv_nsec - times[0].tv_nsec) / 1000000000);
  printf("  sds: %f\n",
      (double) (times[2].tv_sec  - times[1].tv_sec) +
      (double) (times[2].tv_nsec - times[1].tv_nsec) / 1000000000);
  printf("char*: %f\n",
      (double) (times[3].tv_sec  - times[2].tv_sec) +
      (double) (times[3].tv_nsec - times[2].tv_nsec) / 1000000000);


  srand(time(0));
  int n = rand() % count;
  printf("%d %s %s %s\n", n, s_data(&s_strings[n]), sds_strings[n], c_strings[n]);
}
